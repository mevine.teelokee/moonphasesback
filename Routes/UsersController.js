const express = require("express");
const userRouter = express.Router();
const User = require('../Models/User');
const emailUnicityChecker = require('../Utils/emailUnicityChecker')
userRouter.use(express.json());
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const tokenVerify = require("../Utils/verify")

userRouter.post('/user/register', emailUnicityChecker, async (req, res) => {

    const userEmail = req.body.email.toLowerCase()

    const emailExist = await User.findOne({ email: req.body.email.toLowerCase() });
    if (emailExist) return res.status(409).send("Email already exist");

    if (req.body.password === req.body.passwordConfirmation
    ){
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(req.body.password, salt);

        const user = new User({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: userEmail,
            password: hashPassword,
        });
        user.save();
        const token = jwt.sign({ user }, process.env.SECRET);
        res.header("auth-token", token);
        res.json(token)
    }
})


userRouter.get("/user/get", tokenVerify, async (req, res) => {
    let id = req.user.user._id;

    User.findOne({ _id: id })
        .then((user) => res.json(user))
        .catch((err) => res.json(err));
});

userRouter.post("/login", async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).send("Email not found");
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(400).send("Password is not valid");

    const token = jwt.sign({ user }, process.env.SECRET);
    res.header({ "auth-token": token });
    res.json(token);
});



userRouter.get('/user/all', (req, res) => {
    User
        .find()
        .then((users) => res.json(users))
        .catch((err) => res.json(err));
})

userRouter.put('/user/update',tokenVerify, (req, res) => {
    User.findOneAndUpdate({_id: req.user.user._id}, {...req.body})
        .then(() => res.json(`Le profil de ${req.body.first_name} à bien été mis à jour`))
        .catch((err) => res.json(err));
})
userRouter.delete('/user/delete', tokenVerify, async (req, res) => {
    User.findOneAndDelete({_id: req.user.user._id})
        .then(() => res.json('utilisateur supprimé avec succès'))
        .catch(() => res.json('erreur lors de la suppression de l\'utilisateur'))
})


module.exports = userRouter;
