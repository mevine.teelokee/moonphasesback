const express = require("express");
const postRouter = express.Router();
const Post = require('../Models/Post');
postRouter.use(express.json());
// const tokenVerify = require("../Utils/verify")

postRouter.post('/post', (req, res) => {
    // let id = req.user.user._id;
let id = req.body._id
    const post = new Post({
        creationDate: req.body.creationDate,
        postId: req.body.postId,
        content: req.body.content,
        user: id,
    });

    post.save()
        .then(post => res.json(post))
        .catch((err) => res.json(err));
});


postRouter.get('/toto', (req, res) => {
    res.send(`Moon phases backend application running on port`)
})

module.exports = postRouter;
