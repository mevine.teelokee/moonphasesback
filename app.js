const express = require('express');
const mongoose = require('mongoose');
const app = express();
app.use(express.json())
app.use(express.urlencoded({extended: true}));
require("dotenv").config();


const userRouter = require('./Routes/UsersController');
const postRouter = require('./Routes/PostController')

const cors = require("cors");
const port = 4001;
app.use(cors());


app.use("/api", userRouter, postRouter);

app.get('/', (req, res) => {
    res.send(`Moon phases backend application running on port ${port}`)
})
const mongoURI = process.env.MONGODB_URI;
mongoose.connect(mongoURI, {useNewUrlParser: true});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error :"));

app.listen(port, () => {
    console.log(`Moon phase app running on port ${port}`)
})
