const mongoose = require('mongoose');
const {now} = require("mongoose");
const Schema = mongoose.Schema;

const postSchema = new Schema({
    creationDate: {type: Date, default: now()},
    postId: {type: String, required: true},
    content: {type: String, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
});

const Post = mongoose.model('Post', postSchema);

module.exports = Post;
