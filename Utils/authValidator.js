const jwt = require('jsonwebtoken')


const authValidator = (req, res, next) => {
    const token = req.header('authorization')

    !token && res.status(400).send('Access refusé')

    try {
        req.user = jwt.verify(token, process.env.SECRET)
        next()
    } catch (err) {
        res.json(err)
    }
}

module.exports = authValidator
