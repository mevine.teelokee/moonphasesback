const User = require('../Models/User');
const emailUnicityChecker = (req, res, next) => {
    User.findOne({email: req.body.email.toLowerCase()})
        .then((user) => {
            return user ? res.status(409).send('L\'adresse email existe déja') : next()
        })
}
module.exports = emailUnicityChecker
